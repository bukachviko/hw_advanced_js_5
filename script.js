'use strict'
class Card {
    constructor( name, email, title, body, id ) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
    }
    createUserCard(selector) {
        document.querySelector(selector).insertAdjacentHTML('beforeend', `
   
       <div class="userCard" data-post-id="${this.id}">
         <div class="userCard__header">
             <h2>${this.name}</h2>
             <span>${this.email}</span>
             <span class="backet" data-post-id="${this.id}" onclick=deleteCard(this) ><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 30 30" width="30px" height="30px">    <path d="M 14.984375 2.4863281 A 1.0001 1.0001 0 0 0 14 3.5 L 14 4 L 8.5 4 A 1.0001 1.0001 0 0 0 7.4863281 5 L 6 5 A 1.0001 1.0001 0 1 0 6 7 L 24 7 A 1.0001 1.0001 0 1 0 24 5 L 22.513672 5 A 1.0001 1.0001 0 0 0 21.5 4 L 16 4 L 16 3.5 A 1.0001 1.0001 0 0 0 14.984375 2.4863281 z M 6 9 L 7.7929688 24.234375 C 7.9109687 25.241375 8.7633438 26 9.7773438 26 L 20.222656 26 C 21.236656 26 22.088031 25.241375 22.207031 24.234375 L 24 9 L 6 9 z"/></svg></span>
         </div>
        <h4>${this.title}</h4>
        <p>${this.body}</p>
       </div>
`)
    }
}
const createCard = async () => {
    try {
        const {data: users} = await axios.get("https://ajax.test-danit.com/api/json/users");
        const {data: posts} = await axios.get ("https://ajax.test-danit.com/api/json/posts")

        posts.forEach(post => {
            const {title, body, id} = post;
            const {name, email} = users.find(function (el) {
                // console.log(post)
                return el.id === post.userId;
            })
            let card = new Card (name, email, title, body, id)
            card.createUserCard('.container')
        })
    }
    catch(err) {
        console.log(err)
    }
}

createCard();

function deleteCard(backet) {
    console.log(backet.getAttribute('data-post-id'))
    const postId = backet.getAttribute('data-post-id')

    axios.delete("https://ajax.test-danit.com/api/json/posts/"+ postId)
        .then(res => {
            console.log(res)
            if (res.status === 200) {
                document.querySelector(`.userCard[data-post-id="${postId}"]`).remove()
            }
            return res
        })
        .then(data => console.log(data))
}
